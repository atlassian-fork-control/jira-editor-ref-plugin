<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.atlassian.jira.plugins</groupId>
    <artifactId>jira-editor-ref-plugin</artifactId>
    <version>1.0.0-SNAPSHOT</version>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org:atlassian/jira-editor-ref-plugin.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org:atlassian/jira-editor-ref-plugin.git</developerConnection>
        <url>https://bitbucket.org/atlassian/jira-editor-ref-plugin/</url>
    </scm>
    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com/</url>
    </organization>

    <name>JIRA Editor Reference Plugin</name>
    <description>This is the com.atlassian.jira.plugins:jira-editor-ref-plugi for Atlassian JIRA.</description>
    <licenses>
        <license>
            <name>Apache License 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
        </license>
    </licenses>
    <packaging>atlassian-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-core</artifactId>
            <version>${jira.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
            <version>${junit.version}</version>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>atlassian-jira-pageobjects</artifactId>
            <version>${jira.version}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-simple</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira.plugins</groupId>
            <artifactId>jira-editor-pageobjects</artifactId>
            <version>1.2.8</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira</groupId>
            <artifactId>jira-func-tests</artifactId>
            <version>${jira.version}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>com.google.collections</groupId>
                    <artifactId>google-collections</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.google.code.findbugs</groupId>
                    <artifactId>jsr305</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.jira.tests</groupId>
            <artifactId>jira-testkit-plugin</artifactId>
            <version>${testkit.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <product>jira</product>
                    <productVersion>${jira.version}</productVersion>
                    <containerId>${tomcat.version}</containerId>

                    <!-- make AMPS faster -->
                    <enableFastdev>false</enableFastdev>
                    <enableDevToolbox>false</enableDevToolbox>
                    <enablePde>false</enablePde>
                    <skipRestDocGeneration>true</skipRestDocGeneration>
                    <allowGoogleTracking>false</allowGoogleTracking>
                    <skipManifestValidation>true</skipManifestValidation>
                    <extractDependencies>false</extractDependencies>
                    <jvmArgs>-Dcom.atlassian.jira.startup.LauncherContextListener.SYNCHRONOUS=true -Xmx1024m -Xms512m
                    </jvmArgs>

                    <pluginArtifacts>
                        <pluginArtifact>
                            <groupId>${project.groupId}</groupId>
                            <artifactId>${project.artifactId}</artifactId>
                            <version>${project.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.jira.tests</groupId>
                            <artifactId>jira-testkit-plugin</artifactId>
                            <version>${testkit.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.qunit</groupId>
                            <artifactId>atlassian-qunit-plugin</artifactId>
                            <version>${atlassian.qunit.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.jira</groupId>
                            <artifactId>jira-func-test-plugin</artifactId>
                            <version>${jira.version}</version>
                        </pluginArtifact>
                    </pluginArtifacts>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <repositories>
        <repository>
            <id>atlassian-public</id>
            <url>https://maven.atlassian.com/repository/public</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>atlassian-public</id>
            <url>https://maven.atlassian.com/repository/public</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
        </pluginRepository>
    </pluginRepositories>

    <properties>
        <amps.version>6.2.1</amps.version>

        <atlassian.qunit.version>1.1.1</atlassian.qunit.version>
        <hamcrest.version>1.3</hamcrest.version>
        <junit.version>4.12</junit.version>
        <mockito.version>1.10.19</mockito.version>

        <servlet.api.version>3.0.1</servlet.api.version>
        <tomcat.version>tomcat8x</tomcat.version>

        <atlassian.jslibs.version>0.7.0</atlassian.jslibs.version>
        <maven.javadoc.version>2.10.3</maven.javadoc.version>

        <xvfb.enable>true</xvfb.enable>
        <surefire.skip>false</surefire.skip>

        <super.batch.for>none</super.batch.for>
        <atlassian.dev.mode>true</atlassian.dev.mode>

        <ref.plugin.key>com.atlassian.jira.plugins.jira-editor-ref-plugin</ref.plugin.key>

        <testkit.version>7.2.13</testkit.version>
        <jira.version>7.3.0-QR-20161021155936</jira.version>
    </properties>

</project>
