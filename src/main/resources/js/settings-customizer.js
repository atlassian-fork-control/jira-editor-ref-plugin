require(["jira/editor/customizer"], function (EditorCustomizer) {
    EditorCustomizer.customizeSettings(function (tinymceSettings) {
        if (tinymceSettings.plugins.indexOf('textpattern') === -1) {
            tinymceSettings.plugins.push('textpattern');
        }

        tinymceSettings.textpattern_patterns = tinymceSettings.textpattern_patterns || [];
        Array.prototype.push.apply(tinymceSettings.textpattern_patterns, [
            {start: '#', format: 'h1'},
            {start: '##', format: 'h2'},
            {start: '###', format: 'h3'}
        ]);
    });
});