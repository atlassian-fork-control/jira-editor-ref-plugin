require([
    "jquery",
    "jira/util/formatter",
    "jira/editor/registry",
    "jira/editor/customizer"
], function (
    $,
    formatter,
    editorRegistry,
    Customizer
) {
    var RefPlugin = window.RefPlugin;

    Customizer.customizeSettings(function (tinymceSettings, tinymce, SchemaBuilder) {
        SchemaBuilder.withCustomElement('info-macro', ['p', 'ul', 'ol']);
    });

    var INFO = formatter.I18n.getText('refplugin.toolbar.info');
    var INFO_PLACEHOLDER = formatter.I18n.getText('refplugin.macro.info.placeholder');
    var DROPDOWN_ITEM_HTML = '<li><a href="#" data-operation="info">' + INFO + '</a></li>';

    editorRegistry.on('register', function (entry) {
        var $otherDropdown = $(entry.toolbar).find('.wiki-edit-other-picker-trigger');

        $otherDropdown.one('click', function (dropdownClickEvent) {
            var dropdownContentId = dropdownClickEvent.currentTarget.getAttribute('aria-owns');
            var dropdownContent = document.getElementById(dropdownContentId);
            var speechItem = dropdownContent.querySelector('.wiki-edit-speech-item');

            var infoItem = $(DROPDOWN_ITEM_HTML).insertAfter(speechItem).on('click', function () {
                entry.applyIfTextMode(addWikiMarkup).applyIfVisualMode(addRenderedContent);
            });

            entry.onUnregister(function cleanup() {
                infoItem.remove();
            });
        });
    });

    function addWikiMarkup(entry) {
        var wikiEditor = $(entry.textArea).data('wikiEditor');
        var content = wikiEditor.manipulationEngine.getSelection().text || INFO_PLACEHOLDER;
        wikiEditor.manipulationEngine.replaceSelectionWith(RefPlugin.Macros.Info.wiki({content: content}));
    }

    function addRenderedContent(entry) {
        entry.rteInstance.then(function (rteInstance) {
            var tinyMCE = rteInstance.editor;
            if (tinyMCE && !tinyMCE.isHidden()) {
                var content = tinyMCE.selection.getContent() || INFO_PLACEHOLDER;
                tinyMCE.selection.setContent(RefPlugin.Macros.Info.html({ content: '<p>' + content + '</p>' }));
            }
        });
    };
});