package it.com.atlassian.jira.plugins.editor.ref;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetData;
import com.atlassian.jira.pageobjects.framework.util.TimedQueryFactory;
import com.atlassian.jira.plugins.pageobjects.ExtendedViewIssuePage;
import com.atlassian.jira.plugins.pageobjects.editor.DescriptionSectionRte;
import com.atlassian.jira.plugins.pageobjects.editor.RichTextEditor;
import com.atlassian.jira.plugins.pageobjects.editor.RichTextEditor.EditorMode;
import com.atlassian.jira.plugins.pageobjects.editor.ToolbarButton;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;

@WebTest({Category.WEBDRIVER_TEST, Category.ISSUES})
@ResetData
public class TestRefPlugin extends BaseJiraWebTest {

    @Inject
    TimedQueryFactory queryFactory;

    private String issueKey;

    @Before
    public void setUp() throws Exception {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Wiki Style Renderer");
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "description", "Wiki Style Renderer");

        issueKey = backdoor.issues().createIssue("HSP", "xxx").key;
    }

    @Test
    public void testThirdPartyRoundTrip() {
        final DescriptionSectionRte descriptionSectionRte = prepareDescription();
        final RichTextEditor richTextEditor = descriptionSectionRte.getRichTextEditor();
        richTextEditor.switchMode(EditorMode.SOURCE).type("Hello\n\n{footer}Regards{footer}");
        richTextEditor.switchMode(EditorMode.WYSIWYG);

        richTextEditor.type("Hi and ");

        assertThat(richTextEditor.getContent(), containsString("<div class=\"footer-macro\">"));
        assertThat(richTextEditor.getSource(), containsString("{footer}Regards{footer}"));

        descriptionSectionRte.save();

        assertEquals("Hi and \r\n\r\nHello\r\n{footer}Regards{footer}", backdoor.issues().getIssue(issueKey).fields.description);
    }

    @Test
    public void testThirdPartyLegacyRoundTrip() {
        backdoor.plugins().enablePluginModule("com.atlassian.jira.plugins.jira-editor-ref-plugin:header");
        backdoor.issues().setDescription(issueKey, "{header}hello{header}");
        final DescriptionSectionRte descriptionSectionRte = prepareDescription();
        final RichTextEditor richTextEditor = descriptionSectionRte.getRichTextEditor().switchMode(EditorMode.WYSIWYG);

        assertEquals("<pre class=\"external-macro\" data-original-text=\"{header}\" data-command=\"header\" data-new-line-before=\"false\" data-new-line-after=\"false\">hello</pre>", richTextEditor.getContent());

        // leaving <pre> is not immediate
        Poller.waitUntil(queryFactory.forSupplier(() -> {
            richTextEditor.cursorMoveEnd().type(Keys.ARROW_DOWN);
            return richTextEditor.getContent();
        }), containsString("<p>"));

        richTextEditor.type("after");
        assertEquals("<pre class=\"external-macro\" data-original-text=\"{header}\" data-command=\"header\" data-new-line-before=\"false\" data-new-line-after=\"false\">hello</pre><p>after<br data-mce-bogus=\"1\"></p>", richTextEditor.getContent());

        descriptionSectionRte.save();
        assertEquals("{header}hello{header}\r\nafter", backdoor.issues().getIssue(issueKey).fields.description);
        assertEquals("<div class=\"header-macro\"><p>hello</p></div>\n<p>after</p>",
                pageBinder.bind(ExtendedViewIssuePage.class, issueKey).getDescriptionHTML());
    }

    @Test
    public void testToolbarCustomizationInVisualMode() {
        final RichTextEditor richTextEditor = prepareDescription().getRichTextEditor().switchMode(EditorMode.WYSIWYG);

        richTextEditor.clickToolbarButton(new ToolbarButton("Other", ".wiki-edit-other-picker-trigger"), false);
        richTextEditor.clickToolbarButton(new ToolbarButton("Footer", "[data-operation='footer']"), false);

        Poller.waitUntil(richTextEditor.getTimedSource(), startsWith("{footer}Footer...{footer}"));
    }

    @Test
    public void testToolbarCustomizationInTextMode() {
        final RichTextEditor richTextEditor = prepareDescription().getRichTextEditor().switchMode(EditorMode.SOURCE);

        richTextEditor.clickToolbarButton(new ToolbarButton("Other", ".wiki-edit-other-picker-trigger"), false);
        richTextEditor.clickToolbarButton(new ToolbarButton("Footer", "[data-operation='footer']"), false);

        Poller.waitUntil(richTextEditor.getTimedSource(), startsWith("{footer}Footer...{footer}"));
    }

    private DescriptionSectionRte prepareDescription() {
        ExtendedViewIssuePage viewIssuePage = jira.goTo(ExtendedViewIssuePage.class, issueKey);
        Poller.waitUntilTrue("Editable description should show when empty", viewIssuePage.hasEditableDescriptionTimed());

        return viewIssuePage.editDescription(DescriptionSectionRte.class);
    }

}
