AJS.test.require(['com.atlassian.jira.plugins.jira-editor-ref-plugin:handler'], function () {
    var htmlConverter = require('jira/editor/converter');
    var Strings = require('jira/editor/converter/util/strings');

    module('Footer macro Handler');

    test('Test footer macro', function () {
        assertConversion('<div class="footer-macro">footer</div>', '{footer}footer{footer}');
        assertConversion('<div class="footer-macro">footer with <b>Bold</b></div>', '{footer}footer with *Bold*{footer}');
        assertConversion('<p>before</p>\n<div class="footer-macro">\n<p>footer</p>\n</div>', 'before\n{footer}footer{footer}');
        assertConversion('<div class="footer-macro" title="subject">footer with title</div>', '{footer:title=subject}footer with title{footer}');
    });

    var assertConversion = function (html, markup, testName) {
        htmlConverter.convert(html).then(function (result) {
            equal(result, markup, testName);
        }).fail(function (e) {
            throw e;
        });
    };
});