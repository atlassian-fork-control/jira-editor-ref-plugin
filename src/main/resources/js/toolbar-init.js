require([
    "jquery",
    "jira/util/formatter",
    "jira/editor/registry"
], function (
    $,
    formatter,
    editorRegistry
) {
    var RefPlugin = window.RefPlugin;

    var FOOTER = formatter.I18n.getText('refplugin.toolbar.footer');
    var FOOTER_PLACEHOLDER = formatter.I18n.getText('refplugin.macro.footer.placeholder');
    var DROPDOWN_ITEM_HTML = '<li><a href="#" data-operation="footer">' + FOOTER + '</a></li>';

    editorRegistry.on('register', function (entry) {
        var $otherDropdown = $(entry.toolbar).find('.wiki-edit-other-picker-trigger');

        $otherDropdown.one('click', function (dropdownClickEvent) {
            var dropdownContentId = dropdownClickEvent.currentTarget.getAttribute('aria-owns');
            var dropdownContent = document.getElementById(dropdownContentId);
            var speechItem = dropdownContent.querySelector('.wiki-edit-speech-item');

            var footerItem = $(DROPDOWN_ITEM_HTML).insertAfter(speechItem).on('click', function () {
                entry.applyIfTextMode(addWikiMarkup).applyIfVisualMode(addRenderedContent);
            });

            entry.onUnregister(function cleanup() {
                footerItem.remove();
            });
        });
    });

    function addWikiMarkup(entry) {
        var wikiEditor = $(entry.textArea).data('wikiEditor');
        var content = wikiEditor.manipulationEngine.getSelection().text || FOOTER_PLACEHOLDER;
        wikiEditor.manipulationEngine.replaceSelectionWith(RefPlugin.Macros.Footer.wiki({content: content}));
    }

    function addRenderedContent(entry) {
        entry.rteInstance.then(function (rteInstance) {
            var tinyMCE = rteInstance.editor;
            if (tinyMCE && !tinyMCE.isHidden()) {
                var content = tinyMCE.selection.getContent() || FOOTER_PLACEHOLDER;
                tinyMCE.selection.setContent(RefPlugin.Macros.Footer.html({content: content}));
            }
        });
    }
});